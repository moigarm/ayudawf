﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace codeAndDataBase
{
    public class Datos
    {

        private string Conexion = "Data Source = localhost; Initial Catalog = dataBaseConsultas; Integrated Security = true";

        public Datos() { }
        public Datos(int dni, string nombre, int edad, string texto)
        {
            this.dni = dni;
            this.nombre = nombre;
            this.edad = edad;
            this.texto = texto;
        }

        public int dni{ get; set; }
        public string nombre { get; set; }
        public int edad { get; set; }
        public string texto { get; set; }

        public string Insertar(Datos carta)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion;
                SqlCon.Open();
                //Establecer comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "spadd_cliente";
                SqlCmd.CommandType = CommandType.StoredProcedure;

               
                SqlParameter ParNombre = new SqlParameter();
                ParNombre.ParameterName = "@nombre";
                ParNombre.SqlDbType = SqlDbType.VarChar;
                ParNombre.Size = 255;
                ParNombre.Value = carta.nombre;
                SqlCmd.Parameters.Add(ParNombre);


                SqlParameter ParEdad = new SqlParameter();
                ParEdad.ParameterName = "@edad";
                ParEdad.SqlDbType = SqlDbType.Int;
                ParEdad.Value = carta.edad;
                SqlCmd.Parameters.Add(ParEdad);

                //Ejecución del comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se ingresó el Registro";

            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;
        }

        public string Editar(Datos carta)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion;
                SqlCon.Open();
                //Establecer comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "spedit_cliente";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParCarta = new SqlParameter();
                ParCarta.ParameterName = "@dni";
                ParCarta.SqlDbType = SqlDbType.Int;
                ParCarta.Value = carta.dni;
                SqlCmd.Parameters.Add(ParCarta);

                SqlParameter ParNombre = new SqlParameter();
                ParNombre.ParameterName = "@nombre";
                ParNombre.SqlDbType = SqlDbType.VarChar;
                ParNombre.Size = 255;
                ParNombre.Value = carta.nombre;
                SqlCmd.Parameters.Add(ParNombre);

                SqlParameter ParCantidad = new SqlParameter();
                ParCantidad.ParameterName = "@edad";
                ParCantidad.SqlDbType = SqlDbType.Int;
                ParCantidad.Value = carta.edad;
                SqlCmd.Parameters.Add(ParCantidad);

                //Ejecución del comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se actualizó el Registro";

            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;
        }

        public string Eliminar(Datos carta)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion;
                SqlCon.Open();
                //Establecer comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "spdelete_cliente";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParCarta = new SqlParameter();
                ParCarta.ParameterName = "@dni";
                ParCarta.SqlDbType = SqlDbType.Int;
                ParCarta.Value = carta.dni;
                SqlCmd.Parameters.Add(ParCarta);


                //Ejecución del comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se eliminó el Registro";

            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;
        }

        public DataTable Mostrar()
        {
            DataTable DtResultado = new DataTable("Cliente");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion;
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "spmostrar_cliente";
                SqlCmd.CommandType = CommandType.StoredProcedure;


                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
            catch (Exception)
            {
                DtResultado = null;
            }
            return DtResultado;
        }



        public DataTable BuscarNombre(Datos carta)
        {
            DataTable DtResultado = new DataTable("Cliente");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion;
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "spbuscarnombre_cliente";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParTextoBuscar = new SqlParameter();
                ParTextoBuscar.ParameterName = "@texto";
                ParTextoBuscar.SqlDbType = SqlDbType.VarChar;
                ParTextoBuscar.Size = 255;
                ParTextoBuscar.Value = carta.texto;
                SqlCmd.Parameters.Add(ParTextoBuscar);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
            catch (Exception)
            {
                DtResultado = null;
            }
            return DtResultado;
        }

    }
}
