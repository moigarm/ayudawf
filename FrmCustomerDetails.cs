﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace codeAndDataBase
{
    public partial class FrmCustomerDetails : Form
    {
        void FrmCustomer_FormClosed(object sender, FormClosedEventArgs e)
        {
            dgvCustomer.DataSource = Negocio.show();
        }

        public FrmCustomerDetails()
        {
            InitializeComponent();
            
        }


        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void LblSearch_TextChanged(object sender, EventArgs e)
        {
            string texto = txtSearch.Text;
            dgvCustomer.DataSource = Negocio.buscarNombre(texto);
        }
        private void FrmCustomerDetails_Load(object sender, EventArgs e)
        {
            dgvCustomer.DataSource = Negocio.show();
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            FrmCustomer fc = new FrmCustomer();
            fc.edit = false;

            fc.FormClosed += new FormClosedEventHandler(FrmCustomer_FormClosed);

            fc.ShowDialog();

           
        }

        private void Btn_Delate_Click(object sender, EventArgs e)
        {
            int dni = Convert.ToInt32(dgvCustomer.CurrentRow.Cells[0].Value);
            Negocio.delete(dni);
        }

        private void Btn_edit_Click(object sender, EventArgs e)
        {
            int dni = Convert.ToInt32(dgvCustomer.CurrentRow.Cells[0].Value);
            string nombre = dgvCustomer.CurrentRow.Cells[1].Value.ToString();
            int edad = Convert.ToInt32(dgvCustomer.CurrentRow.Cells[2].Value);

            FrmCustomer fc = new FrmCustomer();
            fc.dni = dni;
            fc.nombre = nombre;
            fc.edad = edad;
            fc.edit = true;

            fc.FormClosed += new FormClosedEventHandler(FrmCustomer_FormClosed);

            fc.ShowDialog();

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            string texto = txtSearch.Text;
            dgvCustomer.DataSource = Negocio.buscarNombre(texto);
        }
    }
}
