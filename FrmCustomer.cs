﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using codeAndDataBase;

namespace codeAndDataBase
{
    public partial class FrmCustomer : Form
    {
        public int dni { get; set; }
        public string nombre { get; set; }
        public int edad { get; set; }

        public bool edit { get; set; }

        public FrmCustomer()
        {
            InitializeComponent();
            
        }

        private void FrmCustomer_Load(object sender, EventArgs e)
        {
            if (edit)
            {
                txt_dni.Text = dni.ToString();
                txt_nombre.Text = nombre;
                txt_edad.Text = edad.ToString();
            }
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            string Nombre = ""; int Edad = 1, Dni = 1;
            if (edit) { Dni = Convert.ToInt32(txt_dni.Text); }
            try
            {
                Nombre = txt_nombre.Text;
                Edad = int.Parse(txt_edad.Text);
            }catch ( FormatException)
            {
                MessageBox.Show("Uno de los argumentos está con un formato incorrecto, favor revisar antes de proceder", "Mensaje del sistema", MessageBoxButtons.OK , MessageBoxIcon.Information);
                return;
            }
            if (edit)
            {
                Negocio.update(Dni, Nombre, Edad);
            }
            else { 
                Negocio.add(Nombre, Edad); 
            }
            
            Dispose();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
