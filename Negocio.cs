﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace codeAndDataBase
{
    public class Negocio
    {
        public static string add(string nombre, int edad)
        {
            Datos p = new Datos();
            p.nombre = nombre;
            p.edad = edad;
            return p.Insertar(p);
        }

        public static string update(int dni, string nombre, int edad)
        {
            Datos p = new Datos();
            p.dni = dni;
            p.nombre = nombre;
            p.edad = edad;
            return p.Editar(p);
        }

        public static string delete(int dni)
        {
            Datos p = new Datos();
            p.dni = dni;
            return p.Eliminar(p);
        }

        public static DataTable show()
        {
            return new Datos().Mostrar();
        }

        public static DataTable buscarNombre(string textobuscar)
        {
            Datos p = new Datos();
            p.texto = textobuscar;
            return p.BuscarNombre(p);
        }

    }
}
